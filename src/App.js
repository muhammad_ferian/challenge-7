import LandingPage from "./pages/LandingPage";
import SearchCar from "./pages/SearchCar";
import { Routes, Route, BrowserRouter } from 'react-router-dom'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingPage />} />
        <Route path="/search" element={<SearchCar />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
