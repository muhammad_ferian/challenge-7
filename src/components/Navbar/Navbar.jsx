import { Container, Nav, Navbar, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <div className="header bg-grey">
            <Navbar expand="lg">
                <Container>
                    <Navbar.Brand>
                        <Link to="/">
                            <img src='/images/Rectangle 74.png' alt='logo' />
                        </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="ms-auto">
                            <Nav.Link href="#services">Our Services</Nav.Link>
                            <Nav.Link href="#whyus">Why Us</Nav.Link>
                            <Nav.Link href="">Testimonials</Nav.Link>
                            <Nav.Link href="#FAQ">FAQ</Nav.Link>
                            <Button>Register</Button>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

export default Header;