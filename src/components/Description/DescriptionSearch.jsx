import { Container, Col, Row } from "react-bootstrap";

const Description = () => {
    return (
        <div className="description bg-grey">
            <Container>
                <Row className="mb-section">
                    <Col className="my-auto text-start">
                        <h2 className="fw-bold mb-4">Sewa & Rental Mobil Terbaik di kawasan Blitar</h2>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                    </Col>
                    <Col>
                        <img src="/images/img_car.png" alt="Car Logo" className="img_car" />
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Description;