import { Container, Row, Col, Button } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

const Search = () => {
    const [type, setType] = useState("Pilih Tipe Driver")
    const [date, setDate] = useState("Pilih Waktu")
    const [pickupTime, setPickupTime] = useState("8")
    const [passenger, setPassenger] = useState("")
    const dispatch = useDispatch()
    const state = useSelector((state) => state)

    const handleSubmit = (e) => {
        e.preventDefault()
        if (type !== "Pilih Tipe Driver") {
            const pass = passenger ? passenger : "0"
            const filter = { type, date, pickupTime, pass }
            dispatch((filter))
        }
    }

    const handleType = (e) => {
        setType(e.target.value)
    }

    const handleDate = (e) => {
        setDate(e.target.value)
    }

    const handlePickupTime = (e) => {
        setPickupTime(e.target.value)
    }

    const handlePassenger = (e) => {
        setPassenger(e.target.value)
    }

    useEffect(() => {
    }, [])


    return (
        <Container className="search">
            <Row>
                <Col md='3'>
                    <p>Tipe Driver</p>
                    <select value={type} required onChange={handleType} className="form-select type" name="typeDriver" id="typeDriver">
                        <option disabled hidden>
                            Pilih Tipe Driver
                        </option>
                        <option value="Dengan Sopir">Dengan Sopir</option>
                        <option value="Lepas Sopir">Tanpa Sopir (Lepas Kunci)</option>
                    </select>
                </Col>
                <Col>
                    <p>Tanggal</p>
                    <input className="form-control" onChange={handleDate} required type="date" name="date" id="date" />
                </Col>
                <Col>
                    <p>Waktu Jemput/Ambil</p>
                    <select value={pickupTime} onChange={handlePickupTime} className="form-select time" name="time" id="time">
                        <option value="8">08.00 WIB</option>
                        <option value="9">09.00 WIB</option>
                        <option value="10">10.00 WIB</option>
                        <option value="11">11.00 WIB</option>
                        <option value="12">12.00 WIB</option>
                    </select>
                </Col>
                <Col md='3'>
                    <p>Jumlah Penumpang (optional)</p>
                    <input value={passenger} onChange={handlePassenger} className="form-control" type="text" name="passenger" id="passenger" />
                </Col>
                <Col md='2' className="mt-auto">
                    <Button onClick={() => handleSubmit()}>Cari Mobil</Button>
                </Col>
            </Row>
        </Container>
    )
}

export default Search;