import Navbar from "./Navbar/Navbar";
import Description from "./Description/Description";
import DescriptionSearch from "./Description/DescriptionSearch"
import Search from "./Search/Search";
import Services from "./Services/Services";
import WhyUs from "./WhyUs/WhyUs";
import Rent from "./Rent/Rent";
import FAQ from "./FAQ/FAQ";
import Footer from "./Footer/Footer";
import ListCars from "./ListCars"
import { addCar } from "../features";

export { Navbar, Description, DescriptionSearch, Search, Services, WhyUs, Rent, FAQ, Footer, ListCars, addCar }