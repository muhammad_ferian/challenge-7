import { Col, Container, Row } from "react-bootstrap"
import { Link } from "react-router-dom";

const Footer = () => {
    return (
        <Container className="footer">
            <Row className="mt-5">
                <Col md="4" className="text-start">
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p>binarcarrental@gmail.com</p>
                    <p>081-233-334-808</p>
                </Col>
                <Col md="2">
                    <p>
                        <a href="" className="fw-bold footer-link">Our Service</a>
                    </p>
                    <p>
                        <a href="" className="fw-bold footer-link">Why Us</a>
                    </p>
                    <p>
                        <a href="" className="fw-bold footer-link">Testimonial</a>
                    </p>
                    <p>
                        <a href="" className="fw-bold footer-link">FAQ</a>
                    </p>
                </Col>
                <Col md="4">
                    <p className="text-start">Connect with us</p>
                    <a href="https://www.facebook.com/">
                        <img src="images/icon_facebook.png" alt="icon fb" className="sosmed" />
                    </a>
                    <a href="https://www.instagram.com/">
                        <img src="images/icon_instagram.png" alt="icon ig" className="sosmed" />
                    </a>
                    <a href="https://mail.google.com/">
                        <img src="images/icon_mail.png" alt="icon mail" className="sosmed" />
                    </a>
                    <a href="https://twitter.com/">
                        <img src="images/icon_twitter.png" alt="icon twitter" className="sosmed" />
                    </a>
                    <a href="https://www.twitch.tv/">
                        <img src="images/icon_twitch.png" alt="icon twitch" className="sosmed" />
                    </a>
                </Col>
                <Col md="2">
                    <p className="text-start">Copyright Binar 2022</p>
                    <Link to="/">
                        <img src="/images/Rectangle 74.png" alt="logo" />
                    </Link>
                </Col>
            </Row>
        </Container>
    )
}

export default Footer;