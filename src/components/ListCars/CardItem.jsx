import { Button, Card, Col, Row } from "react-bootstrap";

const CardItem = ({ image, manufacture, model, rentPerDay, description, capacity, transmission, year }) => {
  return (
    <Col md="4" className="mb-4">
      <Card className="shadow">
        <Card.Body>
          <Col className="text-center my-3">
            <div>
              <img src={image} alt={model} className="list-img" />
            </div>
          </Col>
          <p>
            {manufacture} {model}
          </p>
          <p className="fw-bold">Rp {rentPerDay.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")} / hari</p>
          <p className="desc">{description}</p>
          <Col>
            <Row>
              <Col md="1">
                <img src="./images/fi_users.png" alt="users" />
              </Col>
              <Col>
                <p className="feature">{capacity} orang</p>
              </Col>
            </Row>
          </Col>
          <Col>
            <Row>
              <Col md="1">
                <img src="./images/fi_settings.png" alt="settings" />
              </Col>
              <Col>
                <p className="feature">{transmission}</p>
              </Col>
            </Row>
          </Col>
          <Col>
            <Row>
              <Col md="1">
                <img src="./images/fi_calendar.png" alt="calendar" />
              </Col>
              <Col>
                <p className="feature">Tahun {year}</p>
              </Col>
            </Row>
          </Col>
          <Button className="w-100 mt-4">Pilih Mobil</Button>
        </Card.Body>
      </Card>
    </Col>
  )
}

export default CardItem;
