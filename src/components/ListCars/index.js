import { Container, Row } from "react-bootstrap";
import axios from 'axios'
import CardItem from "./CardItem";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from 'react'
import { addCar } from '../index'

const CarList = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    axios
      .get("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json")
      .then((res) => {
        dispatch(addCar(res.data))
      })
      .catch((err) => {
        console.log(err)
      })
  })
  
  const cars = useSelector((state) => state.carsReducer.data);

  return (
    <Container className="list-car">
      <Row>
        {cars.map((car, id) => {
          return <CardItem key={id} {...car} />;
        })}
      </Row>
    </Container>
  )
}

export default CarList;
