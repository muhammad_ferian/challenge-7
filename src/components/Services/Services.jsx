import { Col, Container, Row } from "react-bootstrap";

const Services = () => {
    return (
        <Container className="services mt-5" id="services">
            <Row>
                <Col>
                    <img src="/images/img_service.png" alt="Service" />
                </Col>
                <Col>
                    <h3 className="fw-bold mb-4 mt-5">Best Car Rental for any kind of trip in Blitar!</h3>
                    <p className="text-start">Sewa mobil di Blitar bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                    <Row className="text-start mt-4">
                        <Col sm="1">
                            <img src="/images/Group 53.png" alt="List" />
                        </Col>
                        <Col>
                            <p>Sewa Mobil Dengan Supir di Bali 12 Jam</p>
                        </Col>
                    </Row>
                    <Row className="text-start mt-2">
                        <Col sm="1">
                            <img src="/images/Group 53.png" alt="List" />
                        </Col>
                        <Col>
                            <p>Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                        </Col>
                    </Row>
                    <Row className="text-start mt-2">
                        <Col sm="1">
                            <img src="/images/Group 53.png" alt="List" />
                        </Col>
                        <Col>
                            <p>Sewa Mobil Jangka Panjang Bulanan</p>
                        </Col>
                    </Row>
                    <Row className="text-start mt-2">
                        <Col sm="1">
                            <img src="/images/Group 53.png" alt="List" />
                        </Col>
                        <Col>
                            <p>Gratis Antar - Jemput Mobil di Bandara</p>
                        </Col>
                    </Row>
                    <Row className="text-start mt-2">
                        <Col sm="1">
                            <img src="/images/Group 53.png" alt="List" />
                        </Col>
                        <Col>
                            <p>Layanan Airport Transfer / Drop In Out</p>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    )
}

export default Services;