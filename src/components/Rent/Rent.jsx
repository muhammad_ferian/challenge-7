import { Button, Col, Container, Row } from "react-bootstrap"
import { Link } from "react-router-dom";

const Rent = () => {
    return (
        <Container className="rent mt-5">
            <Row>
                <Col>
                    <h2 className="text-center fw-bold mt-5">Sewa Mobil di Blitar Sekarang</h2>
                </Col>
            </Row>
            <Row className="justify-content-center mt-5">
                <Col md="5">
                    <p className="text-center mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </Col>
            </Row>
            <Row>
                <Col className="text-center mb-5">
                    <Link to="/search">
                        <Button>Mulai Sewa Mobil</Button>
                    </Link>
                </Col>
            </Row>
        </Container>
    )
}

export default Rent;