import { Col, Container, Row, Accordion } from "react-bootstrap"

const FAQ = () => {
    return (
        <Container className="faq mt-5" id="FAQ">
            <Row>
                <Col md="6">
                    <h2 className="fw-bold">Frequently Asked Question</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </Col>
                <Col md="6">
                    <Accordion>
                        <Accordion.Item eventKey="1">
                            <Accordion.Header>
                                Apa saja syarat yang dibutuhkan?
                            </Accordion.Header>
                            <Accordion.Body>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                Eaque eius ea omnis harum fugiat. Quo, architecto veritatis
                            </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="2">
                            <Accordion.Header>
                                Berapa hari minimal sewa mobil lepas kunci?
                            </Accordion.Header>
                            <Accordion.Body>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                Eaque eius ea omnis harum fugiat. Quo, architecto veritatis
                            </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="3">
                            <Accordion.Header>
                                Berapa hari sebelumnya sabaiknya booking sewa mobil?
                            </Accordion.Header>
                            <Accordion.Body>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                Eaque eius ea omnis harum fugiat. Quo, architecto veritatis
                            </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="4">
                            <Accordion.Header>
                                Apakah Ada biaya antar-jemput?
                            </Accordion.Header>
                            <Accordion.Body>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                Eaque eius ea omnis harum fugiat. Quo, architecto veritatis
                            </Accordion.Body>
                        </Accordion.Item>
                        <Accordion.Item eventKey="5">
                            <Accordion.Header>
                                Bagaimana jika terjadi kecelakaan
                            </Accordion.Header>
                            <Accordion.Body>
                                Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                Eaque eius ea omnis harum fugiat. Quo, architecto veritatis
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                </Col>
            </Row>
        </Container>
    )
}

export default FAQ;