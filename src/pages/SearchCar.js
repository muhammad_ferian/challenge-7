import { Navbar, DescriptionSearch, Search, Footer,ListCars } from "../components"

const SearchCar = () => {
  return (
    <>
      <Navbar />
      <DescriptionSearch />
      <Search />
      <ListCars />
      <Footer />
    </>
  );
};

export default SearchCar;