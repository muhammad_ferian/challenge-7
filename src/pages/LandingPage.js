import { Navbar, Description, Services, WhyUs, Rent, FAQ, Footer } from "../components"
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";

const LandingPage = () => {
  return (
    <>
      <Navbar />
      <Description />
      <Services />
      <WhyUs />
      <Rent />
      <FAQ />
      <Footer />
    </>
  );
};

export default LandingPage;